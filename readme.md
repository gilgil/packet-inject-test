packet-inject-test
---

다음 명령어를 통하여 송신되는 beacon frame을 beacon12.pcap 파일로 저장한다(radiotap header의 크기는 12바이트).  

```
mdk3 mon0 b -f ssid.txt
```

beacon12.pcap 파일에서 radiotap header의 present flag 정보를 ghex 툴로 편집하여 radiotap header의 크기를 8로 만들어 beacon8.pcap으로 저장한다.  

beacon12.pcap 파일에는 data rate의 값이 02(1.0 Mb/s) 들어가 있지만 beacon8.pcap 파일에는 data rate의 값이 존재하지 않는다.  

일부 안드로이드 장비(Nexus 6P, Galaxy S7)에서 monitor mode adpater를 이용해서 2.4GHz 영역으로 패킷을 주기적으로 전송하는 경우 data rate의 값이 없어서 패킷 전송이 처음에는 잘 되다가 나중에 가서 crash나는 경우가 있다.  

따라서 802.11 패킷을 전송할 때에는 radiotap header에 data rate값을 설정하고 패킷을 전송하도록 한다.

### references

https://github.com/seemoo-lab/nexmon/blob/master/patches/bcm43596a0/9.96.4_sta_c0/nexmon/src/injection.c#L130
