#include <pcap.h>

#include <iostream>
#include <thread>

using namespace std;

struct Param {
    string fileName_;
    string intfName_;
    int interval_{1000000};
    int count_{-1};

    bool parse(int argc, char** argv) {
        if (argc < 3) return false;
        fileName_ = argv[1];
        intfName_ = argv[2];
        if (argc > 3)
            interval_ = stoi(argv[3]);
        if (argc > 4)
            count_ = stoi(argv[4]);
        return true;
    }

    static void usage() {
        cout << "syntax : packet-inject-test <file name> <interface name> <interval> <count>" << endl;
        cout << "sample : packet-inject-test test.pcap wlan0 1000" << endl;
        cout << "" << endl;
        cout << "interval : default 1,000,000 microseconds" << endl;
        cout << "count    : default -1(infinite)" << endl;
    }
};

int main(int argc, char* argv[]) {
    Param param;
    if (!param.parse(argc, argv)) {
        Param::usage();
        return EXIT_FAILURE;
    }

    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* pcapFile = pcap_open_offline(param.fileName_.data(), errbuf);
    if (pcapFile == nullptr) {
        cerr << "pcap_open_offline return null " << errbuf << endl;
        return EXIT_FAILURE;
    }

    struct pcap_pkthdr* header;
    const u_char* data;
    int res = pcap_next_ex(pcapFile, &header, &data);
    if (res != 1) {
        cerr << "pcap_next_ex return " << res << " " << pcap_geterr(pcapFile) << endl;
        return EXIT_FAILURE;
    }

    pcap_t* pcapDevice = pcap_open_live(param.intfName_.data(), 0, 0, 0, errbuf);
    if (pcapDevice == nullptr) {
        cerr << "pcap_open_live return null " << errbuf << endl;
        return EXIT_FAILURE;
    }

    int count = param.count_;
    while (true) {
        if (count != -1) {
            if (count == 0) break;
            count--;
        }
        res = pcap_sendpacket(pcapDevice, data, header->caplen);
        if (res != 0) {
            cerr << "pcap_sendpacket return " << res << " " << pcap_geterr(pcapDevice) << endl;
            break;
        }
        this_thread::sleep_for(chrono::microseconds(param.interval_));

    }

    pcap_close(pcapFile);
    pcap_close(pcapDevice);
}
